val data_type = spark.conf.get("spark.data.load.type")
val year = spark.conf.get("spark.data.load.year")
val month = spark.conf.get("spark.data.load.month")
val day = spark.conf.get("spark.data.load.day")

val source_path = "/user/dblogs/"+data_type+"/year="+year+"/month="+month+"/day="+day+"/*.parq"
val target_index = "itdb_db-"+data_type+"-"+year+"-"+month+"-"+day

print("Loading data from HDFS path: " + source_path + " to Elasticsearch index: " + target_index)

val day_data = spark.sqlContext.read.parquet(source_path);

val no_uppercase_columns = Set("hostname", "database_type", "database_version", "flume_agent_version", "oracle_sid", "source_type")
val day_data_uppercase = day_data.select(day_data.columns.map(c => col(c).as(if (no_uppercase_columns.contains(c)) c else c.toUpperCase )): _*)

org.elasticsearch.spark.sql.EsSparkSQL.saveToEs(day_data_uppercase, target_index+"/log")

System.exit(0)