#!/bin/bash

SPARK_HOME=/afs/cern.ch/user/d/dblogs/spark-2.1.0-bin-hadoop2.6/
export HADOOP_CONF_DIR=/etc/hadoop/conf

## Compute script path 

# Reference: http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
HOME="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

function show_help {
    echo "Usage: load-data-from-hdfs-to-elastic.sh --data-type <data_type> --year <year> --month <month> --day <day> [--help]"
}

function check_number {
    re='^[0-9]+$'
    
    if ! [[ "$2" =~ $re ]] ; then
       echo "error: $1 is not a number" >&2; 
       echo
       show_help
       echo
       exit 1
    fi
}

## Process arguments ##

while [[ $# -gt 0 ]]
do
    key="$1"

    case $key in
        -t|--data-type)
        data_type="$2"
        shift # past argument
        ;;
        -y|--year)
        year="$2"
        shift # past argument
        ;;
        -m|--month)
        month="$2"
        shift # past argument
        ;;
        -d|--day)
        day="$2"
        shift # past argument
        ;;
        -esp|--es-password)
        es_password="$2"
        shift # past argument
        ;;
        -h|--help)
        show_help
        exit 1
        ;;
    esac
    shift # past argument or value
done

case $data_type in
    audit-11|audit-12|alert|listener|metric)
    ;;
    *)
    echo Data type must be one out of: audit-11, audit-12, alert, listener, metric
    show_help
    exit 1
    ;;
esac

check_number Year $year
check_number Month $month
check_number Day $day

## Execution

[ -f $HOME/elasticsearch-hadoop-5.2.2.jar ] || \
    wget http://central.maven.org/maven2/org/elasticsearch/elasticsearch-hadoop/5.2.2/elasticsearch-hadoop-5.2.2.jar -O $HOME/elasticsearch-hadoop-5.2.2.jar

$SPARK_HOME/bin/spark-shell \
    --jars $HOME/elasticsearch-hadoop-5.2.2.jar \
    --conf spark.ui.port=8989 \
    --conf spark.sql.parquet.binaryAsString=true \
    --conf spark.es.net.ssl="true" \
    --conf spark.es.nodes.wan.only="true" \
    --conf spark.es.nodes="es-itdb.cern.ch" \
    --conf spark.es.port="9203" \
    --conf spark.es.net.http.auth.user="itdb" \
    --conf spark.es.net.http.auth.pass=$es_password \
    --conf spark.data.load.type=$data_type \
    --conf spark.data.load.year=$year \
    --conf spark.data.load.month=$month \
    --conf spark.data.load.day=$day \
    -i $HOME/load-data-from-hdfs-to-elastic.scala
