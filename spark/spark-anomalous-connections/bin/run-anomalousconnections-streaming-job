#!/bin/bash

# Copyright (C) 2016, CERN
# This software is distributed under the terms of the GNU General Public
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".
# In applying this license, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as Intergovernmental Organization
# or submit itself to any jurisdiction.

# Reference: http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

HOME=$SCRIPT_DIR/..

CONFIG_PATH=sparkStreaming/config.properties

$HOME/bin/update_config

export JAVA_HOME=/ORA/dbs01/work/jdk1.8.0_112/
export SPARK_HOME=/ORA/dbs01/work/spark-2.1.0-bin-hadoop2.7/

export HADOOP_CONF_DIR=$HOME/conf/
export SPARK_CONF_DIR=$HOME/conf/

[ -f $HOME/elasticsearch-hadoop-5.2.2.jar ] || \
    wget http://central.maven.org/maven2/org/elasticsearch/elasticsearch-hadoop/5.2.2/elasticsearch-hadoop-5.2.2.jar -O $HOME/elasticsearch-hadoop-5.2.2.jar
    
$SPARK_HOME/bin/spark-submit \
	--master yarn \
	--driver-cores 4 \
	--driver-memory 4G \
	--executor-memory 4G \
	--num-executors 6 \
	--executor-cores 4 \
	--class ch.cern.db.spark.anomalousconnections.Driver \
	--packages org.apache.spark:spark-streaming-kafka-0-10_2.11:2.1.0,org.apache.spark:spark-streaming-flume_2.11:2.1.0 \
	--exclude-packages org.apache.flume:flume-ng-auth \
	--jars $HOME/elasticsearch-hadoop-5.2.2.jar \
	--repositories https://repository.cloudera.com/artifactory/cloudera-repos/ \
	--conf spark.yarn.am.attemptFailuresValidityInterval=1h \
	--conf spark.yarn.max.executor.failures=48 \
	--conf spark.yarn.executor.failuresValidityInterval=1h \
	--conf spark.task.maxFailures=8 \
	--conf spark.hadoop.fs.hdfs.impl.disable.cache=true \
	--conf spark.es.net.ssl="true" \
    --conf spark.es.nodes.wan.only="true" \
    --conf spark.es.nodes="es-itdb.cern.ch" \
    --conf spark.es.port="9203" \
    --conf spark.es.net.http.auth.user="itdb" \
	--conf spark.es.net.http.auth.pass=<ES_PASSWORD>" \
	--principal dblogs@CERN.CH \
	--keytab $HOME/conf/principal.keytab \
	$HOME/target/spark-jobs-*.jar \
	$CONFIG_PATH
