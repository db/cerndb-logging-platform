package ch.cern.db.spark;

import org.junit.Assert;
import org.junit.Test;

public class PropertiesExpirableTest {

	@Test
	public void expiration(){
		Properties.Expirable prop = new Properties.Expirable("src/test/resources/config.properties");
		
		Properties p1 = prop.get();
		Properties p2 = prop.get();
		
		Assert.assertSame(p1, p2);
	}
	
}
