package ch.cern.db.spark.json;

import org.junit.Assert;
import org.junit.Test;

import com.google.gson.JsonObject;

public class JSONObjectsFilterTest {

	@Test
	public void filterTrue() throws Exception{	
		JSONObjectsFilter jsonObjectsFilter = new JSONObjectsFilter("prop2", "val2");
		
		JsonObject jsonObject_ = new JsonObject();
		jsonObject_.addProperty("prop1", "val1");
		jsonObject_.addProperty("prop2", "val2");
		jsonObject_.addProperty("prop3", "val3");
		JSONObject jsonObject = new JSONObject(jsonObject_);
		
		Assert.assertTrue(jsonObjectsFilter.call(jsonObject));
	}
	
	@Test
	public void filterFalse() throws Exception{	
		JSONObjectsFilter jsonObjectsFilter = new JSONObjectsFilter("prop2", "val2");
		
		JsonObject jsonObject_ = new JsonObject();
		jsonObject_.addProperty("prop1", "val1");
		jsonObject_.addProperty("prop2", "val2 no");
		jsonObject_.addProperty("prop3", "val3");
		JSONObject jsonObject = new JSONObject(jsonObject_);
		
		Assert.assertFalse(jsonObjectsFilter.call(jsonObject));
	}
	
	@Test
	public void nullFilter() throws Exception{	
		JSONObjectsFilter jsonObjectsFilter = new JSONObjectsFilter("prop2", null);
		
		JsonObject jsonObject_ = new JsonObject();
		jsonObject_.addProperty("prop1", "val1");
		jsonObject_.addProperty("prop2", "val2 no");
		jsonObject_.addProperty("prop3", "val3");
		JSONObject jsonObject = new JSONObject(jsonObject_);
		
		Assert.assertFalse(jsonObjectsFilter.call(jsonObject));
	}
	
	@Test
	public void nullValue() throws Exception{	
		JSONObjectsFilter jsonObjectsFilter = new JSONObjectsFilter("prop2", "val2");
		
		JsonObject jsonObject_ = new JsonObject();
		jsonObject_.addProperty("prop1", "val1");
		jsonObject_.addProperty("prop2", (String) null);
		jsonObject_.addProperty("prop3", "val3");
		JSONObject jsonObject = new JSONObject(jsonObject_);
		
		Assert.assertFalse(jsonObjectsFilter.call(jsonObject));
	}
	
	@Test
	public void noValue() throws Exception{	
		JSONObjectsFilter jsonObjectsFilter = new JSONObjectsFilter("prop2", "val2");
		
		JsonObject jsonObject_ = new JsonObject();
		jsonObject_.addProperty("prop1", "val1");
		jsonObject_.addProperty("prop3", "val3");
		JSONObject jsonObject = new JSONObject(jsonObject_);
		
		Assert.assertFalse(jsonObjectsFilter.call(jsonObject));
	}
    
    @Test
    public void levelsTrue() throws Exception{  
        String jsonString = "{\"data\":{\"service_name\":\"timtest_s.cern.ch\",\"return_code\":0}}";
        JSONObject jsonObject = new JSONObject.Parser().parse(jsonString.getBytes());
        
        JSONObjectsFilter jsonObjectsFilter = new JSONObjectsFilter("data.service_name", "timtest_s.cern.ch");
        
        Assert.assertTrue(jsonObjectsFilter.call(jsonObject));
    }
    
    @Test
    public void levelsFalseBecauseValue() throws Exception{  
        String jsonString = "{\"data\":{\"service_name\":\"timtest_s.cern.ch\",\"return_code\":0}}";
        JSONObject jsonObject = new JSONObject.Parser().parse(jsonString.getBytes());
        
        JSONObjectsFilter jsonObjectsFilter = new JSONObjectsFilter("data.service_name", "DIFF_VALUE");
        
        Assert.assertFalse(jsonObjectsFilter.call(jsonObject));
    }
    
    @Test
    public void levelsFalseBecauseNoProperty() throws Exception{  
        String jsonString = "{\"data\":{\"service_name\":\"timtest_s.cern.ch\",\"return_code\":0}}";
        JSONObject jsonObject = new JSONObject.Parser().parse(jsonString.getBytes());
        
        JSONObjectsFilter jsonObjectsFilter = new JSONObjectsFilter("data.DOES_NOT_EXIST", "timtest_s.cern.ch");
        Assert.assertFalse(jsonObjectsFilter.call(jsonObject));
        
        jsonObjectsFilter = new JSONObjectsFilter("DOES_NOT_EXIST.DO_NOT_EXIST", "timtest_s.cern.ch");
        Assert.assertFalse(jsonObjectsFilter.call(jsonObject));
    }
	
}
