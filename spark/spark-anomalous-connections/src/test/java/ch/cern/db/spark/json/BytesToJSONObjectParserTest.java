package ch.cern.db.spark.json;

import org.junit.Assert;

import ch.cern.db.spark.json.BytesToJSONObjectParser;
import ch.cern.db.spark.json.JSONObject;
import scala.Tuple2;

public class BytesToJSONObjectParserTest {

    public void parse(){
    
        String json = "{\"metadata.service_name\":\"timtest_s.cern.ch\",\"data.return_code\":0}";
    
        try {
            JSONObject listenerEvent = new BytesToJSONObjectParser()
                    .call(new Tuple2<String, byte[]>(null, json.getBytes()));
            
            Assert.assertNotNull(listenerEvent.getProperty("metadata.service_name"));
            Assert.assertEquals("timtest_s.cern.ch", listenerEvent.getProperty("metadata.service_name"));
            
            Assert.assertNotNull(listenerEvent.getProperty("data.return_code"));
            Assert.assertEquals("0", listenerEvent.getProperty("data.return_code"));
    
        } catch (Exception e) {
            e.printStackTrace();
            
            Assert.fail();
        }
    }
    
}
