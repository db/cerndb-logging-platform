package ch.cern.db.spark.flume;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.util.ManualClock;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FilterFlumeEventsByHeaderTest {

	private JavaStreamingContext ssc;
	private ManualClock clock;
	
	private int batchTime = 1000;

	@Before
	public void setUp() {
		SparkConf conf = new SparkConf()
				.setMaster("local[2]")
				.setAppName("test")
				.set("spark.streaming.clock", "org.apache.spark.streaming.util.ManualClock");

		ssc = new JavaStreamingContext(conf, new Duration(batchTime));
		clock = (ManualClock) ssc.ssc().scheduler().clock();
	}

	@Test
	public void filterListenerEventsTest(){
		
		FlumeEvent event = new FlumeEvent(new HashMap<String, String>(), "body1-1".getBytes());
		event.getHeaders().put("source_type", "listener");
		event.getHeaders().put("other_header", "value");
		
		FilterFlumeEventsByHeader filter = new FilterFlumeEventsByHeader("source_type", "listener");
		
		try {
			Assert.assertTrue(filter.call(event));
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
		
		
		event = new FlumeEvent(new HashMap<String, String>(), "body1-1".getBytes());
		event.getHeaders().put("source_type", "other");
		
		try {
			Assert.assertFalse(filter.call(event));
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
		
	}
	
	@SuppressWarnings("serial")
	@Test
	public void filterListenerEventsWithStreamingTest(){
		
		LinkedList<JavaRDD<FlumeEvent>> queue = new LinkedList<JavaRDD<FlumeEvent>>();
		
		List<FlumeEvent> list_batch1 = new LinkedList<FlumeEvent>();
		FlumeEvent event_1_1 = new FlumeEvent(new HashMap<String, String>(), "body1-1".getBytes());
		event_1_1.getHeaders().put("source_type", "listener");
		event_1_1.getHeaders().put("other_header", "value");
		list_batch1.add(event_1_1);
		FlumeEvent event_1_2 = new FlumeEvent(new HashMap<String, String>(), "body1-2".getBytes());
		event_1_2.getHeaders().put("source_type", "no_listener");
		list_batch1.add(event_1_2);
		queue.add(ssc.sparkContext().parallelize(list_batch1));
		
		List<FlumeEvent> list_batch2 = new LinkedList<FlumeEvent>();
		FlumeEvent event_2_1 = new FlumeEvent(new HashMap<String, String>(), "body2-1".getBytes());
		event_2_1.getHeaders().put("no_source_type", "listener");
		event_2_1.getHeaders().put("other_header", "listener");
		list_batch2.add(event_2_1);
		FlumeEvent event_2_2 = new FlumeEvent(new HashMap<String, String>(), "body2-2".getBytes());
		event_2_2.getHeaders().put("source_type", "listener");
		list_batch2.add(event_2_2);
		queue.add(ssc.sparkContext().parallelize(list_batch2));
		
		List<FlumeEvent> list_batch3 = new LinkedList<FlumeEvent>();
		FlumeEvent event_3_1 = new FlumeEvent(new HashMap<String, String>(), "body3-1".getBytes());
		event_3_1.getHeaders().put("source_type", "listener");
		list_batch3.add(event_3_1);
		FlumeEvent event_3_2 = new FlumeEvent(new HashMap<String, String>(), "body3-2".getBytes());
		event_3_2.getHeaders().put("source_type", "listener");
		list_batch3.add(event_3_2);
		queue.add(ssc.sparkContext().parallelize(list_batch3));
		
		JavaDStream<FlumeEvent> inputEvents = ssc.queueStream(queue);
		
		JavaDStream<FlumeEvent> producedEvents = FilterFlumeEventsByHeader.apply(inputEvents, "source_type", "listener");
		
		final List<FlumeEvent> producedEvents_list = Collections.synchronizedList(new LinkedList<FlumeEvent>());
		producedEvents.foreachRDD(new VoidFunction<JavaRDD<FlumeEvent>>() {
			public void call(JavaRDD<FlumeEvent> rdd) throws Exception {
				producedEvents_list.addAll(rdd.collect());
			}
		});
		
		ssc.start();
		
		clock.advance(batchTime);
		try {
			Thread.sleep(batchTime);
		} catch (InterruptedException e) {}
		Assert.assertEquals(1, producedEvents_list.size());
		Assert.assertEquals(event_1_1, producedEvents_list.remove(0));
		
		clock.advance(batchTime);
		try {
			Thread.sleep(batchTime);
		} catch (InterruptedException e) {}
		Assert.assertEquals(1, producedEvents_list.size());
		Assert.assertEquals(event_2_2, producedEvents_list.remove(0));
		
		clock.advance(batchTime);
		try {
			Thread.sleep(batchTime);
		} catch (InterruptedException e) {}
		Assert.assertEquals(2, producedEvents_list.size());
		Assert.assertEquals(event_3_1, producedEvents_list.remove(0));
		Assert.assertEquals(event_3_2, producedEvents_list.remove(0));
	}

	@After
	public void tearDown() {
		ssc.stop();
		ssc = null;
	}

}
