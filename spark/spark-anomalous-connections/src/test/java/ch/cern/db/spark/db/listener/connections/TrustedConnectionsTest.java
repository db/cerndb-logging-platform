package ch.cern.db.spark.db.listener.connections;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import ch.cern.db.spark.anomalousconnections.DataMineProperties;
import ch.cern.db.spark.db.listener.connections.DataMineProperty.Type;

public class TrustedConnectionsTest {

	private static DataMineProperties dataMineProperties = null;
	
	@BeforeClass
	public static void setUp(){
		dataMineProperties = new DataMineProperties();
		
		dataMineProperties.add(new DataMineProperty("client_host", 1f, Type.EQUAL));
		dataMineProperties.add(new DataMineProperty("client_user", 1f, Type.EQUAL));
		dataMineProperties.add(new DataMineProperty("client_program", 1f, Type.EQUAL));
		dataMineProperties.add(new DataMineProperty("service_name", 1f, Type.EQUAL));
		dataMineProperties.add(new DataMineProperty("day_of_the_week", 1f, Type.NUMERIC, 7-1));
		dataMineProperties.add(new DataMineProperty("hour_of_the_day", 1f, Type.NUMERIC, 24*60-1));
		dataMineProperties.add(new DataMineProperty("event_timestamp", 0f, Type.EQUAL));
	}

	@Test
	public void shouldBeTrusted() throws IOException {
		TrustedConnections trustedConnections = new TrustedConnections(dataMineProperties.getList());;
		
		Connection con1 = new Connection();
		con1.properties.put("client_host", "srv-c2f38-23-01");
		con1.properties.put("client_user", "trackerpro");
		con1.properties.put("client_program", "perl");
		con1.properties.put("service_name", "int9r_lb.cern.ch");
		con1.properties.put("day_of_the_week", "1");
		con1.properties.put("hour_of_the_day", "502");
		trustedConnections.add(con1);
		
		Connection con2 = new Connection();
		con2.properties.put("client_host", "srv-c2f38-23-01");
		con2.properties.put("client_user", "trackerpro");
		con2.properties.put("client_program", "perl");
		con2.properties.put("service_name", "int9r_lb.cern.ch");
		con2.properties.put("day_of_the_week", "9");
		con2.properties.put("hour_of_the_day", "502");
		trustedConnections.add(con2);
		
		
		Connection newConn = new Connection();
		newConn.properties.put("client_host", "srv-c2f38-23-01");
		newConn.properties.put("client_user", "trackerpro");
		newConn.properties.put("client_program", "perl");
		newConn.properties.put("service_name", "int9r_lb.cern.ch");
		newConn.properties.put("day_of_the_week", "4");
		newConn.properties.put("hour_of_the_day", "508");
		
		assertNull(trustedConnections.shouldBeTrusted(dataMineProperties, newConn));
	}
	
	@Test
	public void shouldNotBeTrusted() throws IOException {
		TrustedConnections trustedConnections = new TrustedConnections(dataMineProperties.getList());;
		
		Connection con1 = new Connection();
		con1.properties.put("client_host", "srv-c2f38-23-01");
		con1.properties.put("client_user", "trackerpro");
		con1.properties.put("client_program", "perl-2");
		con1.properties.put("service_name", "int9r_lb.cern.ch");
		con1.properties.put("day_of_the_week", "4");
		con1.properties.put("hour_of_the_day", "502");
		trustedConnections.add(con1);
		
		Connection con2 = new Connection();
		con2.properties.put("client_host", "srv-c2f38-23-01");
		con2.properties.put("client_user", "trackerpro");
		con2.properties.put("client_program", "perl");
		con2.properties.put("service_name", "int9r_lb.cern.ch");
		con2.properties.put("day_of_the_week", "1");
		con2.properties.put("hour_of_the_day", "502");
		trustedConnections.add(con2);
		
		Connection newConn = new Connection();
		newConn.properties.put("client_host", "srv-c2f38-23-01");
		newConn.properties.put("client_user", "trackerpro");
		newConn.properties.put("client_program", "perl");
		newConn.properties.put("service_name", "int9r_lb.cern.ch");
		newConn.properties.put("day_of_the_week", "5");
		newConn.properties.put("hour_of_the_day", "5");
		
		AnomalousConnection anomaluosConnection = trustedConnections.shouldBeTrusted(dataMineProperties, newConn);
		assertNotNull(anomaluosConnection);
		assertEquals(16.86, anomaluosConnection.getClosestDistance(), 0.01);
	}
	
	@Test
	public void emptyTrustedConnections() throws IOException {
		TrustedConnections trustedConnections = new TrustedConnections(dataMineProperties.getList());;

		Connection newConn = new Connection();
		newConn.properties.put("client_host", "srv-c2f38-23-01");
		newConn.properties.put("client_user", "trackerpro");
		newConn.properties.put("client_program", "perl");
		newConn.properties.put("service_name", "int9r_lb.cern.ch");
		newConn.properties.put("day_of_the_week", "5");
		newConn.properties.put("hour_of_the_day", "5");
		
		assertNull(trustedConnections.shouldBeTrusted(dataMineProperties, newConn));
	}
	
	@Test
	public void comparator() throws IOException {
		TrustedConnections trustedConnections = new TrustedConnections(dataMineProperties.getList());
		
		Connection con1 = new Connection();
		con1.properties.put("client_host", "srv-c2f38-23-01");
		con1.properties.put("client_user", "trackerpro");
		con1.properties.put("client_program", "perl");
		con1.properties.put("service_name", "int9r_lb.cern.ch");
		con1.properties.put("day_of_the_week", "1");
		con1.properties.put("hour_of_the_day", "502");
		con1.properties.put("event_timestamp", "timestamp1");
		trustedConnections.add(con1);
		
		//Equal to previous connection
		Connection con2 = new Connection();
		con2.properties.put("client_host", "srv-c2f38-23-01");
		con2.properties.put("client_user", "trackerpro");
		con2.properties.put("client_program", "perl");
		con2.properties.put("service_name", "int9r_lb.cern.ch");
		con2.properties.put("day_of_the_week", "1");
		con2.properties.put("hour_of_the_day", "502");
		con2.properties.put("event_timestamp", "timestamp2");
		trustedConnections.add(con2);
		
		//Different one
		Connection con3 = new Connection();
		con3.properties.put("client_host", "host123");
		con3.properties.put("client_user", "trackerpro");
		con3.properties.put("client_program", "perl");
		con3.properties.put("service_name", "NAME");
		con3.properties.put("day_of_the_week", "1");
		con3.properties.put("hour_of_the_day", "502");
		con3.properties.put("event_timestamp", "timestamp3");
		trustedConnections.add(con3);
		
		// Time stamp is not taken into account
		// So, the first two are equals
		
		assertEquals(2, trustedConnections.connections.size());
	}

}
