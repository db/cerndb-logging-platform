package ch.cern.db.spark.db.listener.connections;

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gson.JsonObject;

import ch.cern.db.spark.anomalousconnections.DataMineProperties;
import ch.cern.db.spark.db.listener.connections.DataMineProperty.Type;
import ch.cern.db.spark.json.JSONObject;

public class ConnectionTest {
	
	private static DataMineProperties dataMineProperties = null;
	
	@BeforeClass
	public static void setUp(){
		dataMineProperties = new DataMineProperties();
		
		dataMineProperties.add(new DataMineProperty("client_host", 1f, Type.EQUAL));
		dataMineProperties.add(new DataMineProperty("client_program", 1f, Type.EQUAL));
		dataMineProperties.add(new DataMineProperty("hour_of_the_day", 1f, Type.NUMERIC, 24-1));
	}

	@Test
	public void distanceBetweenEmpty() {
		Connection con1 = new Connection();
		Connection con2 = new Connection();
		
		assertEquals(0f, con1.distance(dataMineProperties, con2), 0f);
	}
	
	@Test
	public void distanceBetweenEquals() {
		Connection con1 = new Connection();
		con1.properties.put("client_host", "ABCD.cern.ch");
		con1.properties.put("client_program", "JDBC");
		
		Connection con2 = new Connection();
		con2.properties.put("client_host", "ABCD.cern.ch");
		con2.properties.put("client_program", "JDBC");
		
		assertEquals(0f, con1.distance(dataMineProperties, con2), 0f);
	}
	
	@Test
	public void distanceBetweenValueAndNull() {
		Connection con1 = new Connection();
		con1.properties.put("client_host", "ABCD.cern.ch");
		con1.properties.put("client_program", "JDBC");
		
		Connection con2 = new Connection();
		con2.properties.put("client_host", "ABCD.cern.ch");
		con2.properties.put("client_program", null);
		
		float weight = getDataMinePropertyByName("client_program").getWeight();
		
		assertEquals((weight / dataMineProperties.getMaximumDistance()) * Connection.MAXIMUM_STANDARDIZED_DISTANCE, 
				con1.distance(dataMineProperties, con2), 0.1f);
	}
	
	private DataMineProperty getDataMinePropertyByName(String name) {
		for (DataMineProperty prop : dataMineProperties.getList())
			if(prop.getPropertyName().equals(name))
				return prop;
			
		throw new RuntimeException("DataMineProperty with name (" + name + ") does not exist");
	}

	@Test
	public void distanceBetweenValueAndNoProperty() {
		Connection con1 = new Connection();
		con1.properties.put("client_host", "ABCD.cern.ch");
		con1.properties.put("client_program", "JDBC");
		
		Connection con2 = new Connection();
		con2.properties.put("client_host", "ABCD.cern.ch");
		
		float weight = getDataMinePropertyByName("client_program").getWeight();

		assertEquals((weight / dataMineProperties.getMaximumDistance()) * Connection.MAXIMUM_STANDARDIZED_DISTANCE, 
				con1.distance(dataMineProperties, con2), 0.1f);
	}
	
	@Test
	public void distanceBetweenEqualsNumericValues() {
		Connection con1 = new Connection();
		con1.properties.put("client_host", "ABCD.cern.ch");
		con1.properties.put("hour_of_the_day", "123");
		
		Connection con2 = new Connection();
		con2.properties.put("client_host", "ABCD.cern.ch");
		con2.properties.put("hour_of_the_day", "123");
		
		assertEquals(0f, con1.distance(dataMineProperties, con2), 0.1f);
	}
	
	@Test
	public void distanceMaximumDiferentNumericValues() {
		Connection con1 = new Connection();
		con1.properties.put("client_host", "ABCD.cern.ch");
		con1.properties.put("hour_of_the_day", "11");
		
		Connection con2 = new Connection();
		con2.properties.put("client_host", "ABCD.cern.ch");
		con2.properties.put("hour_of_the_day", "1500");
		
		float weight = getDataMinePropertyByName("hour_of_the_day").getWeight();
		
		assertEquals((weight / dataMineProperties.getMaximumDistance()) * Connection.MAXIMUM_STANDARDIZED_DISTANCE, 
				con1.distance(dataMineProperties, con2), 0.01f);
	}

	@Test
	public void distanceMoreThanMaximumDiferentNumericValues() {
		Connection con1 = new Connection();
		con1.properties.put("client_host", "ABCD.cern.ch");
		con1.properties.put("hour_of_the_day", "11");
		
		Connection con2 = new Connection();
		con2.properties.put("client_host", "ABCD.cern.ch");
		con2.properties.put("hour_of_the_day", "999999");
		
		float weight = getDataMinePropertyByName("hour_of_the_day").getWeight();
		
		assertEquals((weight / dataMineProperties.getMaximumDistance()) * Connection.MAXIMUM_STANDARDIZED_DISTANCE, 
				con1.distance(dataMineProperties, con2), 0.1f);
	}
	
	@Test
	public void extraPropertiesFromTimestamps() {
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty(Connection.TIMESTAMP_PROPERTY_NAME, "2016-12-08T15:19:09+0100");
		
		List<String> props = new LinkedList<String>();
		props.add(Connection.TIMESTAMP_PROPERTY_NAME);
		Connection con1 = Connection.parse(props , new JSONObject(jsonObject));
		
		assertEquals("5", con1.properties.get("day_of_the_week"));
		assertEquals("15", con1.properties.get("hour_of_the_day"));
	}
	
}
