package ch.cern.db.spark;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class Properties extends java.util.Properties{
	
	private static final long serialVersionUID = 2510326766802151233L;
	
	public static class Expirable extends ObjectExpirable<Properties> implements Serializable{
		private static final long serialVersionUID = -5361682529035003933L;
		
		private String path;
		
		public Expirable(String path) {
			this.path = path;
		}

		@Override
		protected Properties loadObject() throws IOException {
			Properties props = new Properties();
			
	        FileSystem fs = FileSystem.get(new Configuration());

	        InputStreamReader is = new InputStreamReader(fs.open(new Path(path)));
			
	        props.load(is);

			is.close();

			return props;
		}
	}

}
