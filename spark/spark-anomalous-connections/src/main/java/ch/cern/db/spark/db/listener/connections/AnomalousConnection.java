package ch.cern.db.spark.db.listener.connections;

public class AnomalousConnection extends Connection {

    private static final long serialVersionUID = -3184596021315745857L;
    
    float closest_distance;
    
    public AnomalousConnection(Connection connection, float closest_distance) {
        this.properties = connection.properties;
        this.closest_distance = closest_distance;
    }

    public float getClosestDistance() {
        return closest_distance;
    }

}
