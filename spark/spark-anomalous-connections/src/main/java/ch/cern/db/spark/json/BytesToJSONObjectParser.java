package ch.cern.db.spark.json;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;

import scala.Tuple2;

public class BytesToJSONObjectParser implements Function<Tuple2<String, byte[]>, JSONObject> {

    private static final long serialVersionUID = -1872409083281227230L;

    private JSONObject.Parser parser = new JSONObject.Parser();
    
    public JSONObject call(Tuple2<String, byte[]> tuple) throws Exception {
        return parser.parse(tuple._2);
    }

    public static JavaDStream<JSONObject> apply(JavaPairDStream<String, byte[]> stream) {
        return stream.map(new BytesToJSONObjectParser());
    }

}
