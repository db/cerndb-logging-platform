package ch.cern.db.spark.anomalousconnections;

import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import ch.cern.db.spark.ObjectExpirable;
import ch.cern.db.spark.Properties;
import ch.cern.db.spark.db.listener.connections.Connection;
import ch.cern.db.spark.db.listener.connections.DataMineProperty;
import ch.cern.db.spark.db.listener.connections.DataMineProperty.Type;

public class DataMineProperties {
	
	public static final String DISTANCE_THRESHOLD_PARAM = "distance.threshold";
	public static final float DISTANCE_THRESHOLD_DEFAULT = Connection.MAXIMUM_STANDARDIZED_DISTANCE / 10;
	private float distanceThreshold = DISTANCE_THRESHOLD_DEFAULT;
	
	public static final String DATA_MINE_PROPERTIES_PARAM = "datamineproperties";
	public static final String DATA_MINE_PROPERTIES_NUMBER_PARAM = "number";
	public static final String DATA_MINE_PROPERTIES_PROPERTY_NAME_PARAM = "name";
	public static final String DATA_MINE_PROPERTIES_WEIGHT_PARAM = "weight";
	public static final String DATA_MINE_PROPERTIES_COMPARISON_TYPE_PARAM = "comparisontype";
	public static final String DATA_MINE_PROPERTIES_MAXIMUM_DISTANCE_PARAM = "maximumdistance";
	
	private List<DataMineProperty> dataMineProperties;

	private float maximumDistance;
	
	public DataMineProperties(){
		dataMineProperties = new LinkedList<DataMineProperty>();
	}

	public static DataMineProperties parse(Properties props) throws IOException {
		DataMineProperties dmprop = new DataMineProperties();
		
		dmprop.distanceThreshold = Float.parseFloat(props.getProperty(
				DISTANCE_THRESHOLD_PARAM, 
				Float.toString(DISTANCE_THRESHOLD_DEFAULT)));
		
		dmprop.dataMineProperties = new LinkedList<DataMineProperty>();
		
		int num = Integer.parseInt(props.getProperty(DATA_MINE_PROPERTIES_PARAM + "." + DATA_MINE_PROPERTIES_NUMBER_PARAM));
		
		for (int i = 0; i < num; i++) {
			String property_name = props.getProperty(
					DATA_MINE_PROPERTIES_PARAM + "." + i + "." + DATA_MINE_PROPERTIES_PROPERTY_NAME_PARAM);
			float weight = Float.parseFloat(props.getProperty(
					DATA_MINE_PROPERTIES_PARAM + "." + i + "." + DATA_MINE_PROPERTIES_WEIGHT_PARAM,
					"1"));
			String comparison_type = props.getProperty(
					DATA_MINE_PROPERTIES_PARAM + "." + i + "." + DATA_MINE_PROPERTIES_COMPARISON_TYPE_PARAM,
					Type.EQUAL.toString()).toUpperCase();
			float maximum_distance = Float.parseFloat(props.getProperty(
					DATA_MINE_PROPERTIES_PARAM + "." + i + "." + DATA_MINE_PROPERTIES_MAXIMUM_DISTANCE_PARAM,
					"-1"));
			
			dmprop.dataMineProperties.add(new DataMineProperty(property_name, weight, Type.valueOf(comparison_type), maximum_distance));
		}
		
		float acum = 0;
		for (DataMineProperty dataMineProperty : dmprop.dataMineProperties)
			acum += dataMineProperty.getWeight();
		dmprop.maximumDistance = acum;
		
		return dmprop;
	}
	
	public void add(DataMineProperty dataMineProperty){
		dataMineProperties.add(dataMineProperty);
		maximumDistance += dataMineProperty.getWeight();
	}

	public float getDistanceThreshold() {
		return distanceThreshold;
	}

	public List<DataMineProperty> getList() {
		return dataMineProperties;
	}

	public float getMaximumDistance() {
		return maximumDistance;
	}

	public static class Expirable extends ObjectExpirable<DataMineProperties> implements Serializable{
		private static final long serialVersionUID = -1735022463988200108L;
		
		private Properties.Expirable props;
		
		public Expirable(Properties.Expirable props){
			this.props = props;
		}

		@Override
		protected DataMineProperties loadObject() throws IOException {
			return DataMineProperties.parse(props.get());
		}
	}
	
}
