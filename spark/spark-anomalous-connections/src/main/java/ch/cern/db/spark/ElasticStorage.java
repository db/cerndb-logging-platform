package ch.cern.db.spark;

import java.io.Serializable;
import java.util.Map;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.elasticsearch.spark.streaming.api.java.JavaEsSparkStreaming;

import ch.cern.db.spark.db.listener.connections.AnomalousConnection;
import scala.Tuple2;

public class ElasticStorage implements Serializable{

    private static final long serialVersionUID = 3227711984536257061L;
    
    private String index;

    public ElasticStorage(String index) {        
        this.index = index + "/log";
    }

    //http://spark.apache.org/docs/2.1.0/sql-programming-guide.html#interoperating-with-rdds
    public void save(JavaDStream<Tuple2<AnomalousConnection, Integer>> dStream) {
        JavaDStream<Map<String, String>> elasticDataRDD = dStream.map(new Function<Tuple2<AnomalousConnection,Integer>, Map<String, String>>() {
            private static final long serialVersionUID = 980650977703156599L;

            public Map<String, String> call(Tuple2<AnomalousConnection, Integer> tuple) throws Exception {
                return tuple._1.getProperties();
            }
        });
        
        JavaEsSparkStreaming.saveToEs(elasticDataRDD, index);
    }

}
