package ch.cern.db.spark.db.listener.connections;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;

import com.google.common.annotations.VisibleForTesting;

import ch.cern.db.spark.anomalousconnections.DataMineProperties;

public class TrustedConnections implements Serializable{

	private static final long serialVersionUID = 4697028919697040848L;

	@VisibleForTesting
	protected TreeSet<Connection> connections;
	
	public TrustedConnections(List<DataMineProperty> dataMineProperties) {
		connections = new TreeSet<Connection>(new Connection.Comparator_(dataMineProperties));
	}

	public void add(Connection connection) {
		connections.add(connection);
	}
	
	public TreeSet<Connection> getConnections(){
		return connections;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("TrustedConnections [\n");
		
		for (Connection connection : connections)
			sb.append(connection + "\n");
		
		sb.append("]\n");
				
		return sb.toString();
	}

	/**
	 * Connection will be trusted only if the difference with any of the known connections
	 * is less than distance_threshold
	 * @param dataMineProperties 
	 * @param distanceThreshold 
	 * 
	 * @param connection
	 * @return true: connection should be trusted, false: connection should not be trusted
	 */
	public AnomalousConnection shouldBeTrusted(DataMineProperties dataMineProperties, Connection connection) {
		
		//We trust the first connection
		if(connections.size() == 0)
			return null;
		
		float minimun_distance = Connection.MAXIMUM_STANDARDIZED_DISTANCE;
		
		long initTime = System.currentTimeMillis();
		
		for (Connection trustedConnection : connections) {
			float distance = connection.distance(dataMineProperties, trustedConnection);
			
			if(distance < minimun_distance)
				minimun_distance = distance;
		}
		
		long finishTime = System.currentTimeMillis();
		long elapsedTime = finishTime - initTime;
		
		if(elapsedTime > 5)
			System.err.println(new Date() 
				+ " - Distance calculation elapsed time: " + elapsedTime + " ms"
				+ " - Trusted connections size: " + connections.size());
		
		if(minimun_distance < dataMineProperties.getDistanceThreshold())
		    return null;
		else
		    return new AnomalousConnection(connection, minimun_distance);
	}

}
