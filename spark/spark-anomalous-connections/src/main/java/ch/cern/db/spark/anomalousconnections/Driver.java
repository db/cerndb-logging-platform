package ch.cern.db.spark.anomalousconnections;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.BytesDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.utils.Bytes;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.Function0;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;

import ch.cern.db.spark.ElasticStorage;
import ch.cern.db.spark.ObjectExpirable;
import ch.cern.db.spark.Properties;
import ch.cern.db.spark.db.listener.connections.AnomalousConnection;
import ch.cern.db.spark.db.listener.connections.Connection;
import ch.cern.db.spark.db.listener.connections.ConnectiosParserFunction;
import ch.cern.db.spark.db.listener.connections.DataMineProperty;
import ch.cern.db.spark.db.listener.connections.DetectAnomaluosConnections;
import ch.cern.db.spark.db.listener.connections.TrustedConnections;
import ch.cern.db.spark.flume.FilterFlumeEventsByHeader;
import ch.cern.db.spark.flume.FlumeEvent;
import ch.cern.db.spark.flume.FlumeEventsParser;
import ch.cern.db.spark.flume.JSONObjectParser;
import ch.cern.db.spark.json.BytesToJSONObjectParser;
import ch.cern.db.spark.json.JSONObject;
import ch.cern.db.spark.json.JSONObjectsExtractor;
import ch.cern.db.spark.json.JSONObjectsFilter;
import scala.Tuple2;

public final class Driver {

	public Driver() {
	}

	public static void main(String[] args) throws IOException {

		final Properties.Expirable props = new Properties.Expirable(args[0]);

		final SparkConf sparkConf = new SparkConf().setAppName("AnomalousConnectionsStreamingJob");
		configureKryoSerializer(sparkConf);
		final Duration batchDuration = Durations.seconds(5 * 60);
		final String checkpointDirectory = props.get().getProperty("checkpoint_directory");
		
		final Set<String> kafkaTopics = new HashSet<String>(Arrays.asList(props.get().getProperty("topics").split(",")));
		
		final Map<String, Object> kafkaParams = new HashMap<String, Object>();
		kafkaParams.put("bootstrap.servers", props.get().getProperty("brokers"));
		kafkaParams.put("group.id", "spark_knn_anomalous_connections");
		
		JavaStreamingContext ssc = JavaStreamingContext.getOrCreate(
				checkpointDirectory, 
				new Function0<JavaStreamingContext>() {
			
			private static final long serialVersionUID = 52346365653464L;

			public JavaStreamingContext call() throws Exception {
				return createNewStreamingContext(
						sparkConf, 
						checkpointDirectory, 
						batchDuration,
						kafkaParams,
						kafkaTopics, 
						props);
			}
		});

		// Start the computation
		ssc.start();
		try {
			ssc.awaitTermination();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	protected static JavaStreamingContext createNewStreamingContext(
			SparkConf sparkConf, 
			String checkpointDirectory,
			Duration batchDuration, 
			Map<String, Object> kafkaParams, 
			Set<String> kafkaTopics, 
			Properties.Expirable props) throws IOException {
	    
		JavaStreamingContext ssc = new JavaStreamingContext(sparkConf, batchDuration);	
		ssc.checkpoint(checkpointDirectory);
		
		// Create direct Kafka stream with brokers and topics
		JavaPairDStream<String, byte[]> messages = createKafkaInputStream(
				ssc, 
				kafkaTopics, 
				kafkaParams);

		JavaDStream<JSONObject> jsonEvents = BytesToJSONObjectParser.apply(messages);
		JavaDStream<JSONObject> listenerEvents = JSONObjectsFilter.apply(jsonEvents, "metadata.source_type", "listener");
		JavaDStream<JSONObject> establishEvents = JSONObjectsFilter.apply(listenerEvents, "data.type", "establish");
		JavaDStream<JSONObject> dataFromEstablishEvents = JSONObjectsExtractor.apply(establishEvents, "data");
		
		JavaDStream<Connection> newConnections = ConnectiosParserFunction.apply(props, dataFromEstablishEvents);
		
		JavaDStream<Tuple2<AnomalousConnection, Integer>> anomalousConnections = 
				DetectAnomaluosConnections.apply(ssc, props, newConnections);

		String targetIndex = props.get().getProperty("anomalous_connections.elastic.index");
		
		ElasticStorage elasticStorage = new ElasticStorage(targetIndex);
		elasticStorage.save(anomalousConnections);

		return ssc;
	}
	
	private static JavaPairDStream<String, byte[]> createKafkaInputStream(JavaStreamingContext ssc,
			Set<String> kafkaTopics, Map<String, Object> kafkaParams) {
		
		kafkaParams.put("key.deserializer", StringDeserializer.class);
		kafkaParams.put("value.deserializer", BytesDeserializer.class);

		JavaInputDStream<ConsumerRecord<String, Bytes>> inputStream = KafkaUtils.createDirectStream(
				ssc,
			    LocationStrategies.PreferConsistent(),
			    ConsumerStrategies.<String, Bytes>Subscribe(kafkaTopics, kafkaParams));
		
		return inputStream.mapToPair(new PairFunction<ConsumerRecord<String, Bytes>, String, byte[]>() {
				private static final long serialVersionUID = -6395124310608129936L;
	
				public Tuple2<String, byte[]> call(ConsumerRecord<String, Bytes> record) {
					return new Tuple2<String, byte[]>(record.key(), record.value().get());
				}
			});
	}

	private static void configureKryoSerializer(SparkConf sparkConf) {
		sparkConf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
		
		// Hold the largest object you will serialize.
		sparkConf.set("spark.kryoserializer.buffer", "16m");
		
		//Obligate to register all classes
		//sparkConf.set("spark.kryo.registrationRequired", "true");
		
		Class<?>[] serializableClasses = {
				//Java specific
				java.util.HashMap.class,
				Object[].class,
				//Spark specific
				scala.collection.mutable.WrappedArray.ofRef.class,
				org.apache.spark.streaming.rdd.MapWithStateRDDRecord.class,
				org.apache.spark.streaming.util.OpenHashMapBasedStateMap.class,
				//Project specific
				Connection.class,
				ConnectiosParserFunction.class,
				DataMineProperties.class,
				DataMineProperty.class,
				DetectAnomaluosConnections.class,
				FlumeEvent.class,
				FlumeEventsParser.class,
				FilterFlumeEventsByHeader.class,
				JSONObject.class,
				JSONObjectsFilter.class,
				JSONObjectParser.class,
				ObjectExpirable.class,
				Properties.class,
				TrustedConnections.class,
			};
		
		sparkConf.registerKryoClasses(serializableClasses);
	}

}
