package ch.cern.db.spark.db.listener.connections;

import org.apache.spark.api.java.Optional;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function3;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.State;
import org.apache.spark.streaming.StateSpec;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaMapWithStateDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;

import ch.cern.db.spark.Properties;
import ch.cern.db.spark.anomalousconnections.DataMineProperties;
import scala.Tuple2;

public class DetectAnomaluosConnections 
	implements Function3<String, Optional<Connection>, State<TrustedConnections>, Tuple2<AnomalousConnection, Integer>>{

	private static final long serialVersionUID = -6164480571908671230L;
	
	private DataMineProperties.Expirable dataMineProperties;
	
	private DetectAnomaluosConnections(Properties.Expirable props){
		dataMineProperties = new DataMineProperties.Expirable(props);
	}

	public Tuple2<AnomalousConnection, Integer> call(String user, Optional<Connection> connectionOpt, State<TrustedConnections> state)
			throws Exception {

		if(!connectionOpt.isPresent())
			return null;
		
		Connection connection = connectionOpt.get();
		
		TrustedConnections trustedConnections = state.exists() ? state.get() : new TrustedConnections(dataMineProperties.get().getList());
		
		AnomalousConnection anomalousConnection = trustedConnections.shouldBeTrusted(dataMineProperties.get(), connection);
		
		if(anomalousConnection == null){
			trustedConnections.add(connection);
			
			state.update(trustedConnections);
			
			return null;
		}else{
			return new Tuple2<AnomalousConnection, Integer>(
			            anomalousConnection, trustedConnections.getConnections().size());
		}
	}
	
	public static JavaDStream<Tuple2<AnomalousConnection, Integer>> apply(
			JavaStreamingContext ssc, 
			Properties.Expirable props, 
			JavaDStream<Connection> newConnections) {
		
		JavaPairDStream<String, Connection> newConnectionsWithUser = newConnections
				.mapToPair(new PairFunction<Connection, String, Connection>() {
			private static final long serialVersionUID = 939448986210853005L;

			public Tuple2<String, Connection> call(Connection connection) throws Exception {
				return new Tuple2<String, Connection>(connection.getProperty("client_user"), connection);
			}
		});
		
		JavaMapWithStateDStream<String, 
			Connection, 
			TrustedConnections, 
			Tuple2<AnomalousConnection, Integer>> anomalousConnections = 
				newConnectionsWithUser.mapWithState(
						StateSpec.function(new DetectAnomaluosConnections(props)));
		
		anomalousConnections.checkpoint(Durations.minutes(10));
		
		return anomalousConnections.filter(new Function<Tuple2<AnomalousConnection, Integer>, Boolean>() {
			private static final long serialVersionUID = 631549922973051212L;

			public Boolean call(Tuple2<AnomalousConnection, Integer> tuple) throws Exception {
				return tuple != null;
			}
		});
	}

}
