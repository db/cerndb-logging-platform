package ch.cern.db.spark.db.listener.connections;

import static ch.cern.db.spark.anomalousconnections.DataMineProperties.DATA_MINE_PROPERTIES_NUMBER_PARAM;
import static ch.cern.db.spark.anomalousconnections.DataMineProperties.DATA_MINE_PROPERTIES_PARAM;
import static ch.cern.db.spark.anomalousconnections.DataMineProperties.DATA_MINE_PROPERTIES_PROPERTY_NAME_PARAM;

import java.util.LinkedList;
import java.util.List;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.streaming.api.java.JavaDStream;

import ch.cern.db.spark.Properties;
import ch.cern.db.spark.json.JSONObject;

public class ConnectiosParserFunction implements Function<JSONObject, Connection> {

	private static final long serialVersionUID = -4800203164769897650L;
	
	/**
	 * Properties which will be extracted from ListenerEvent
	 */
	private List<String> propertiesNames;

	public ConnectiosParserFunction(List<String> propertiesNames) {
		this.propertiesNames = propertiesNames;
	}

	public Connection call(JSONObject listenerEvent) throws Exception {
		return Connection.parse(propertiesNames, listenerEvent);
	}
	
	public static JavaDStream<Connection> apply(Properties.Expirable props, JavaDStream<JSONObject> establishEvents) {
		List<String> dataMinePropertiesNames = getDataMinePropertiesNames(props);
		
		return establishEvents.map(new ConnectiosParserFunction(dataMinePropertiesNames));
	}
	
	private static List<String> getDataMinePropertiesNames(Properties.Expirable props) {
		List<String> propertyNames = new LinkedList<String>();
		
		int num = Integer.parseInt(props.get().getProperty(DATA_MINE_PROPERTIES_PARAM + "." + DATA_MINE_PROPERTIES_NUMBER_PARAM));
		
		for (int i = 0; i < num; i++)
			propertyNames.add(props.get().getProperty(DATA_MINE_PROPERTIES_PARAM + "." + i + "." + DATA_MINE_PROPERTIES_PROPERTY_NAME_PARAM));
		
		return propertyNames;
	}

}
