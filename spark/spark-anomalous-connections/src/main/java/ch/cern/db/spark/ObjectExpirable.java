package ch.cern.db.spark;

import java.io.IOException;

/**
 * Represents an object which is not serialized, so it needs to be reloaded in every batch
 * 
 * @author dlanza
 *
 * @param <T> Object type
 */
public abstract class ObjectExpirable<T> {

	private T object;
	
	protected ObjectExpirable(){
	}
	
	public T get(){
		if(object == null)
			try {
				object = loadObject();
			} catch (IOException e) {
				object = null;
				
				e.printStackTrace();
			}
		
		return object;
	}

	protected abstract T loadObject() throws IOException;
	
}
