package ch.cern.db.spark.json;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.streaming.api.java.JavaDStream;

public class JSONObjectsFilter implements Function<JSONObject, Boolean> {

	private static final long serialVersionUID = -3892453818249278634L;
	
	private String propertyName;
	
	private String value;

	public JSONObjectsFilter(String propertyName, String value) {
		this.propertyName = propertyName;
		this.value = value;
	}

	public Boolean call(JSONObject jsonObject) throws Exception {
	    return filter(propertyName, jsonObject);
	}
	
	private boolean filter(String propertyName, JSONObject jsonObject) {
	    if(propertyName.contains(".")){
	        String topPropertyName = propertyName.substring(0, propertyName.indexOf('.')); 
	        JSONObject topObject = jsonObject.getElement(topPropertyName);
	        
	        if(topObject == null)
	            return false;
	        else
	            return filter(propertyName.substring(propertyName.indexOf('.') + 1), topObject);
	    }
	    
        if(jsonObject.getProperty(propertyName) == null)
            if(value == null)
                return true;
            else
                return false;
        
        if(value == null)
            return false;
        
        return jsonObject.getProperty(propertyName).equals(value);
    }

    public static JavaDStream<JSONObject> apply(JavaDStream<JSONObject> jsonObjects, String propertyName, String value) {
		return jsonObjects.filter(new JSONObjectsFilter(propertyName, value));
	}

}
