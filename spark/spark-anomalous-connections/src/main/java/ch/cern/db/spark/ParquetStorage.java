package ch.cern.db.spark;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.VoidFunction2;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.streaming.Time;
import org.apache.spark.streaming.api.java.JavaDStream;

import ch.cern.db.spark.anomalousconnections.DataMineProperties;
import ch.cern.db.spark.db.listener.connections.AnomalousConnection;
import ch.cern.db.spark.db.listener.connections.DataMineProperty;
import scala.Tuple2;

public class ParquetStorage implements Serializable{

    private static final long serialVersionUID = 3227711984536257061L;
    
    private SparkConf sparkConf;
    private StructType schema;
    private String path;

    public ParquetStorage(SparkConf sparkConf, String path, DataMineProperties dataMineProperties) {
        List<StructField> fields = new ArrayList<StructField>();
        
        for (DataMineProperty dataMinePropertie : dataMineProperties.getList())
            fields.add(DataTypes.createStructField(
                    dataMinePropertie.getPropertyName(), 
                    DataTypes.StringType, 
                    true));
        
        fields.add(DataTypes.createStructField(
                "closest_distance", 
                DataTypes.FloatType, 
                true));
        
        this.sparkConf = sparkConf;
        this.schema = DataTypes.createStructType(fields);
        this.path = path;
    }

    //http://spark.apache.org/docs/2.1.0/sql-programming-guide.html#interoperating-with-rdds
    public void save(JavaDStream<Tuple2<AnomalousConnection, Integer>> dStream) {
        dStream.foreachRDD(new VoidFunction2<JavaRDD<Tuple2<AnomalousConnection,Integer>>, Time>() {
            private static final long serialVersionUID = 5190787651191236101L;

            public void call(JavaRDD<Tuple2<AnomalousConnection, Integer>> rdd, Time time) throws Exception {
                
                JavaRDD<AnomalousConnection> anomalousConnectionsRDD = 
                        rdd.map(new Function<Tuple2<AnomalousConnection,Integer>, AnomalousConnection>() {
                    private static final long serialVersionUID = -6835827030894491021L;

                    public AnomalousConnection call(Tuple2<AnomalousConnection, Integer> tuple) throws Exception {
                        return tuple._1();
                    }
                });
                
                JavaRDD<Row> rowRDD = anomalousConnectionsRDD.map(new Function<AnomalousConnection, Row>() {
                    private static final long serialVersionUID = 5324634632L;

                    public Row call(AnomalousConnection anomalousConnection) throws Exception {
                        Object[] attributes = new Object[schema.size()];
                        
                        int i = 0;
                        String[] fieldNames = schema.fieldNames();
                        for (; i < fieldNames.length - 1; i++) 
                            attributes[i] = anomalousConnection.getProperty(fieldNames[i]);
                        
                        attributes[i] = anomalousConnection.getClosestDistance();
                            
                        return RowFactory.create(attributes);
                    }
                });
                
                SparkSession sparkSession = SparkSession
                        .builder()
                        .config(sparkConf)
                        .getOrCreate();
                
                Dataset<Row> df = sparkSession.createDataFrame(rowRDD, schema);
                
                df.repartition(1).write().format("parquet").save(path + "/" + time.milliseconds());
            }
        });
    }

}
