package ch.cern.db.spark.db.listener.connections;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import ch.cern.db.spark.anomalousconnections.DataMineProperties;
import ch.cern.db.spark.json.JSONObject;

public class Connection implements Serializable{

	private static final long serialVersionUID = 4694984945212513247L;
	
	public static float MAXIMUM_STANDARDIZED_DISTANCE = 100f;
	
	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
	public static final String TIMESTAMP_PROPERTY_NAME = "event_timestamp";
		
	protected HashMap<String, String> properties = new HashMap<String, String>();

	protected Connection(){
	}

	public HashMap<String, String> getProperties() {
        return properties;
    }
	
	public String getProperty(String property) {
		return properties.get(property);
	}
	
	@Override
	public String toString() {
		return "Connection [properties=" + properties + "]";
	}

	public static Connection parse(List<String> propertiesNames, JSONObject jsonObject) {
		Connection connection = new Connection(); 
		
		for (String propertyName : propertiesNames)
			connection.properties.put(propertyName, jsonObject.getProperty(propertyName));
		
		addCustomProperties(connection.properties);
		
		return connection;
	}
	
	private static void addCustomProperties(HashMap<String, String> properties) {
		
		String timestamp_st = properties.get(TIMESTAMP_PROPERTY_NAME);
		try {
			Date timestamp = DATE_FORMAT.parse(timestamp_st);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(timestamp);
			
			properties.put("day_of_the_week", Integer.toString(calendar.get(Calendar.DAY_OF_WEEK)));
			properties.put("hour_of_the_day", Integer.toString(calendar.get(Calendar.HOUR_OF_DAY)));
		} catch (Exception e) {}
		
	}

	/**
	 * Calculate the standardized distance between this and another Connection
	 * @param dataMineProperties 
	 * 
	 * @param other Other Connection
	 * @return decimal value from 0 to MAXIMUM_STANDARDIZED_DISTANCE
	 */
	public float distance(DataMineProperties dataMineProperties, Connection other) {
		float acumulated_distance = 0;
		
		for (DataMineProperty propertie : dataMineProperties.getList())
			acumulated_distance += propertie.distance(
					properties.get(propertie.getPropertyName()), 
					other.properties.get(propertie.getPropertyName()));
		
		return (acumulated_distance / dataMineProperties.getMaximumDistance()) * MAXIMUM_STANDARDIZED_DISTANCE;
	}
	
	public static class Comparator_ implements Comparator<Connection>, Serializable {

		private static final long serialVersionUID = -5163022967679557895L;
		
		private List<DataMineProperty> properties;

		public Comparator_(List<DataMineProperty> dataMineProperties) {
			this.properties = dataMineProperties;
		}

		public int compare(Connection conn1, Connection conn2) {
			if (conn1 == conn2)
				return 0;
			
			for (DataMineProperty property : properties) {
			    if(property.getWeight() == 0F)
			        continue;
			    
				String conn1_val = conn1.getProperty(property.getPropertyName());
				String conn2_val = conn2.getProperty(property.getPropertyName());
				
				if(conn1_val != null && conn2_val == null)
					return -1;
				if(conn1_val == null && conn2_val != null)
					return 1;
				if(conn1_val != null && conn2_val != null){
					int comp_result = conn1_val.compareTo(conn2_val);
					
					if(comp_result != 0)
						return comp_result;
				}
					
			}
			
			return 0;
		}
		
	}

	public void write(Output output) {
		output.writeInt(properties.size(), true);
		
		for (Map.Entry<String, String> property : properties.entrySet()) {
			output.writeString(property.getKey());
			output.writeString(property.getValue());
		}
	}

	public static Connection read(Input input) {
		Connection connection = new Connection();
		
		int size = input.readInt(true);
		
		for (int i = 0; i < size; i++) {
			String key = input.readString();
			String value = input.readString();
			
			connection.properties.put(key, value);
		}
		
		return connection;
	}

}
