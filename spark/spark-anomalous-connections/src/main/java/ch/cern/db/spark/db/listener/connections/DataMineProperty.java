package ch.cern.db.spark.db.listener.connections;

import java.io.Serializable;

public class DataMineProperty implements Serializable{

	private static final long serialVersionUID = -5781320186587798776L;

	public enum Type{EQUAL, NUMERIC};
	
	private String propertyName;
	private float weight;
	private Type comparison_type;
	
	/**
	 * If comparison_type is NUMERIC, we need to maximum distance between the two number that
	 * will be compared in order to calculate the distance between 0 and weight
	 */
	private float maximun_difference;

	public DataMineProperty(String propertyName) {
		this(propertyName, 1f, Type.EQUAL, -1f);
	}
	
	public DataMineProperty(String propertyName, float weight) {
		this(propertyName, weight, Type.EQUAL, -1f);
	}
	
	public DataMineProperty(String propertyName, float weight, Type comparison_type) {
		this(propertyName, weight, comparison_type, -1f);
	}
	
	public DataMineProperty(String propertyName, float weight, Type comparison_type, float maximun_difference) {
		if(comparison_type == Type.NUMERIC && maximun_difference < 0f)
			throw new RuntimeException("If type is " + Type.NUMERIC + ", maximun_difference needs to be specified.");
		
		this.propertyName = propertyName;
		this.weight = weight;
		this.comparison_type = comparison_type;
		this.maximun_difference = maximun_difference;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public float getWeight() {
		return weight;
	}

	public Type getComparisonType() {
		return comparison_type;
	}

	public float getMaximunDifference() {
		return maximun_difference;
	}
    
	/**
	 * Calculate the distance between the two values
	 * 
	 * If comparison_type is EQUAL, this method return 0 if both values are equal, otherwise return weight
	 * If comparison_type is NUMERIC, values have to be numbers, this method return a number between 0 and weight 
	 * proportional to the distance. Returned value will be: 
	 * 		- 0 in the case both values are equal
	 * 		- weight in the case the difference is equal or higher than maximun_difference 
	 * 
	 * @param value1 First value to compare
	 * @param value2 Second value to compare
	 * @return decimal value between 0 and weight
	 */
	public float distance(String value1, String value2) {
		switch (comparison_type) {
		case EQUAL:
			if (value1 == null){
				if (value2 != null)
					return weight;
			}else if (!value1.equals(value2))
				return weight;
			
			break;
		case NUMERIC:
			float num_value1 = value1 != null ? Float.parseFloat(value1) : 0f;
			float num_value2 = value2 != null ? Float.parseFloat(value2) : 0f;
			
			float difference = Math.min(Math.abs(num_value1 - num_value2), maximun_difference);
			
			return (difference / maximun_difference) * weight;
		}
		
		return 0f;
	}

    @Override
    public String toString() {
        return "DataMineProperty [propertyName=" + propertyName + ", weight=" + weight + ", comparison_type="
                + comparison_type + ", maximun_difference=" + maximun_difference + "]";
    }
	
}
