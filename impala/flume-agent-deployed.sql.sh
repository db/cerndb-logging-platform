
echo 'drop table dblogs.hosts_whitelist;'
echo 'create table dblogs.hosts_whitelist (hostname STRING, sid STRING);'

cat flume-agent-deployed.hosts | while read line
do
	sid=`echo $line | cut -d' ' -f1 | cut -d'_' -f1 | tr [a-z] [A-Z]`
	hostname=`echo $line | cut -d' ' -f2`.cern.ch

	echo 'INSERT INTO dblogs.hosts_whitelist VALUES ("'$hostname'", "'$sid'");'
done