#!/bin/bash

# Copyright (C) 2016, CERN
# This software is distributed under the terms of the GNU General Public
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".
# In applying this license, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as Intergovernmental Organization
# or submit itself to any jurisdiction.

# Reference: http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

mkdir -p $SCRIPT_DIR/log/

echo "============== METRIC ============="
date >> $SCRIPT_DIR/log/metric.log
$SCRIPT_DIR/partition-table-data.sh metric END_TIME >> $SCRIPT_DIR/log/metric.log 2>&1
tail -14 $SCRIPT_DIR/log/metric.log
echo

echo "============== LISTENER ============="
date >> $SCRIPT_DIR/log/listener.log
$SCRIPT_DIR/partition-table-data.sh listener event_timestamp >> $SCRIPT_DIR/log/listener.log 2>&1
tail -14 $SCRIPT_DIR/log/listener.log
echo

echo "============== ALERT ============="
date >> $SCRIPT_DIR/log/alert.log
$SCRIPT_DIR/partition-table-data.sh alert ORIGINATING_TIMESTAMP >> $SCRIPT_DIR/log/alert.log 2>&1
tail -14 $SCRIPT_DIR/log/alert.log
echo

echo "============== AUDIT 11 ============="
date >> $SCRIPT_DIR/log/audit-11.log
$SCRIPT_DIR/partition-table-data.sh audit-11 TIMESTAMP >> $SCRIPT_DIR/log/audit-11.log 2>&1
tail -14 $SCRIPT_DIR/log/audit-11.log
echo

echo "============== AUDIT 12 ============="
date >> $SCRIPT_DIR/log/audit-12.log
$SCRIPT_DIR/partition-table-data.sh audit-12 EVENT_TIMESTAMP >> $SCRIPT_DIR/log/audit-12.log 2>&1
tail -14 $SCRIPT_DIR/log/audit-12.log
echo