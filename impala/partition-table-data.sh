
if [ "$1" = "" ]
then
        echo Data type must be specified as first argument
        exit
else
	DATA_TYPE=$1
fi

if [ "$2" = "" ]
then
        echo Time column must be specified as second argument
        exit
else
        TIME_COLUMN=$2
fi

ROOT_DIR=/user/dblogs/$DATA_TYPE/
TABLE=`echo dblogs.$DATA_TYPE | tr - _`

hdfs dfs -mkdir -p $ROOT_DIR/processing/

hdfs dfs -mv $ROOT_DIR/recent/*.parquet $ROOT_DIR/processing/

SAMPLE_FILE=`hdfs dfs -ls $ROOT_DIR/processing/ | tail -1 | awk '{print $8}'`
if [ "$SAMPLE_FILE" = "" ]
then
	echo Processing directory "$ROOT_DIR/processing/" is empty, exiting...
	exit
fi

impala-shell -q " \
CREATE TABLE "$TABLE"_processing \
	LIKE PARQUET '"$SAMPLE_FILE"' \
	STORED AS parquet \
	LOCATION '"$ROOT_DIR"/processing/'; \
\
INSERT INTO "$TABLE" \
	PARTITION (year, month, day) \
	SELECT *, \
		year(from_unixtime(cast(unix_timestamp(\`"$TIME_COLUMN"\`) as BIGINT))), \
		month(from_unixtime(cast(unix_timestamp(\`"$TIME_COLUMN"\`) as BIGINT))), \
		day(from_unixtime(cast(unix_timestamp(\`"$TIME_COLUMN"\`) as BIGINT))) \
		FROM "$TABLE"_processing; \
\
DROP TABLE "$TABLE"_processing; \
"

