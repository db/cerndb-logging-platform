if [ "$1" = "" ]
then
        echo Data type must be specified as first argument
        exit
else
	DATA_TYPE=$1
fi

ROOT_DIR=/user/dblogs/$DATA_TYPE/
TABLE=`echo dblogs.$DATA_TYPE | tr - _`

SAMPLE_FILE=`hdfs dfs -ls $ROOT_DIR/recent/*.parquet | tail -1 | awk '{print $8}'`
if [ "$SAMPLE_FILE" = "" ]
then
	echo Directory "$ROOT_DIR"/recent/ is empty, exiting...
	exit
fi

impala-shell -q " \
CREATE EXTERNAL TABLE "$TABLE" \
	LIKE PARQUET '"$SAMPLE_FILE"' \
	PARTITIONED BY (year int, month int, day int) \
	STORED AS parquet \
	LOCATION '"$ROOT_DIR"'; \
"