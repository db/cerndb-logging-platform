package ch.cern.db.flume.source.reader.log;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedList;

import org.apache.flume.Context;
import org.apache.flume.Event;

import ch.cern.db.flume.JSONEvent;
import ch.cern.db.log.LogEvent;
import ch.cern.db.utils.SUtils;

public class ListenerLogEventParser implements LogEventParser{
	
	public static SimpleDateFormat LISTENER_LOGS_DATE_FORMAT = new SimpleDateFormat("dd-MMM-yyy HH:mm:ss");
	public static SimpleDateFormat ELASTICSEARCH_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

	private static HashMap<String, String> name_mapping = new HashMap<String, String>(); 
	static{
		name_mapping.put("CONNECT_DATA_CID_PROGRAM", "client_program");
		name_mapping.put("CONNECT_DATA_CID_HOST", "client_host");
		name_mapping.put("CONNECT_DATA_CID_USER", "client_user");
		name_mapping.put("ADDRESS_PROTOCOL", "client_protocol");
		name_mapping.put("ADDRESS_HOST", "client_ip");
		name_mapping.put("ADDRESS_PORT", "client_port");
	}
	
	public ListenerLogEventParser(Context context) {
	}

	@Override
	public Event parse(LogEvent logEvent) {
		JSONEvent jsonEvent = new JSONEvent();
		
		String[] fields = logEvent.getText().split(" \\* "); 
		
		jsonEvent.addProperty("event_timestamp", ELASTICSEARCH_DATE_FORMAT.format(logEvent.getTimestamp()));
		jsonEvent.addProperty("text", logEvent.getText());
		
		if(isPingType(fields))
			return parsePingType(jsonEvent, fields);
		else if (isStatusType(fields))
			return parseStatusType(jsonEvent, fields);
		else if (isServiceUpdateType(fields))
			return parseServiceUpdateType(jsonEvent, fields);
		else if (isEstablishType(fields))
			return parseEstablishType(jsonEvent, fields);
		else if (isServicesType(fields))
			return parseServicesType(jsonEvent, fields);
		else
			return jsonEvent;
	}

	private boolean isServicesType(String[] fields) {
		if(fields.length == 4 && fields[3] != null)
			return fields[2].equals("services");
		else
			return false;
	}

	private Event parseServicesType(JSONEvent jsonEvent, String[] fields) {
		addProperties(jsonEvent, fields[1]);
		jsonEvent.addProperty("type", fields[2]);
		jsonEvent.addProperty("return_code", Integer.parseInt(fields[3]));
		System.out.println(jsonEvent.getJsonObject().toString());
		return jsonEvent;
	}
	
	private boolean isEstablishType(String[] fields) {
		if(fields.length == 6 && fields[3] != null)
			return fields[3].equals("establish");
		else
			return false;
	}

	private Event parseEstablishType(JSONEvent jsonEvent, String[] fields) {
		addProperties(jsonEvent, fields[1]);
		addProperties(jsonEvent, fields[2]);
		jsonEvent.addProperty("type", fields[3]);
		jsonEvent.addProperty("service_name", fields[4]);
		jsonEvent.addProperty("return_code", Integer.parseInt(fields[5]));
		
		return jsonEvent;
	}

	private boolean isServiceUpdateType(String[] fields) {
		if(fields.length == 4 && fields[1] != null)
			return fields[1].equals("service_update");
		else
			return false;
	}

	private Event parseServiceUpdateType(JSONEvent jsonEvent, String[] fields) {
		jsonEvent.addProperty("type", fields[1]);
		jsonEvent.addProperty("service_name", fields[2]);
		jsonEvent.addProperty("return_code", Integer.parseInt(fields[3]));
		
		return jsonEvent;
	}

	private boolean isStatusType(String[] fields) {
		if(fields.length == 4 && fields[2] != null)
			return fields[2].equals("status");
		else
			return false;
	}

	private Event parseStatusType(JSONEvent jsonEvent, String[] fields) {
		addProperties(jsonEvent, fields[1]);
		jsonEvent.addProperty("type", fields[2]);
		jsonEvent.addProperty("return_code", Integer.parseInt(fields[3]));
		
		return jsonEvent;
	}

	private boolean isPingType(String[] fields) {
		if(fields.length == 3 && fields[1] != null)
			return fields[1].equals("ping");
		else
			return false;
	}

	private Event parsePingType(JSONEvent jsonEvent, String[] fields) {
		jsonEvent.addProperty("type", fields[1]);
		jsonEvent.addProperty("return_code", Integer.parseInt(fields[2]));
		
		return jsonEvent;
	}
	
	private void addProperties(JSONEvent jsonEvent, String source_string) {
		LinkedList<String> entities = new LinkedList<String>();
		
		String acum = "";
		boolean equal = false;
		
		char[] chars = source_string.toCharArray();
		for (char c : chars) {
			if(c == '('){
				acum = "";
			}else if(c == ')'){
				if(equal)
					jsonEvent.addProperty(mapName(SUtils.join(entities, '_')), mayInt(acum));
				equal = false;
				
				entities.removeLast();
			}else if(c == '='){
				entities.add(acum);
				acum = "";
				
				equal = true;
			}else{ 
				acum += c;
			}
		}
	}

	private String mapName(String original_name) {
		return name_mapping.containsKey(original_name) ? 
				name_mapping.get(original_name) : original_name;
	}

	private Object mayInt(String text) {
		try{
			return Integer.parseInt(text);
		}catch(Exception e){
			return text;
		}
	}
	
	/**
	 * Builder which builds new instance of this class
	 */
	public static class Builder implements LogEventParser.Builder {

		@Override
		public LogEventParser build(Context context) {
			return new ListenerLogEventParser(context);
		}

	}

}
