/**
 * Copyright (C) 2016, CERN
 * This software is distributed under the terms of the GNU General Public
 * Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".
 * In applying this license, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

package ch.cern.db.flume.source.reader.log;

import java.io.File;
import java.text.ParseException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.flume.Channel;
import org.apache.flume.ChannelSelector;
import org.apache.flume.Context;
import org.apache.flume.channel.ChannelProcessor;
import org.apache.flume.channel.MemoryChannel;
import org.apache.flume.channel.ReplicatingChannelSelector;
import org.apache.flume.conf.Configurables;
import org.apache.flume.source.PollableSourceRunner;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import com.google.gson.JsonObject;

import ch.cern.db.flume.JSONEvent;
import ch.cern.db.flume.source.DropDuplicatedEventsProcessor;
import ch.cern.db.flume.source.LogFileSource;
import ch.cern.db.flume.source.reader.ReliableLogFileEventReader;
import ch.cern.db.log.LogEvent;

/**
 * 
 * 
 * @author daniellanzagarcia
 *
 */
public class ListenerLogEventParserTest {

	@Test
	public void establish_extractProperties() throws ParseException{
		
		String text = "02-NOV-2016 09:12:09 "
				+ "* (CONNECT_DATA="
					+ "(SERVER=DEDICATED)"
					+ "(SERVICE_NAME=timtest_s.cern.ch)"
					+ "(FAILOVER_MODE="
						+ "(TYPE=SELECT)"
						+ "(METHOD=BASIC)"
						+ "(RETRIES=200)"
						+ "(DELAY=15))"
					+ "(CID="
						+ "(PROGRAM=perl)"
						+ "(HOST=itrac50044.cern.ch)"
						+ "(USER=oracle))"
					+ "(INSTANCE_NAME=ACCINT2)) "
				+ "* (ADDRESS="
					+ "(PROTOCOL=tcp)"
					+ "(HOST=10.16.8.163)"
					+ "(PORT=57543)) "
				+ "* establish "
				+ "* timtest_s.cern.ch "
				+ "* 0";
		
		LogEvent event = new LogEvent(ListenerLogEventParser.LISTENER_LOGS_DATE_FORMAT, text);
		
		ListenerLogEventParser parser = 
				(ListenerLogEventParser) new ListenerLogEventParser.Builder().build(null);
		
		JSONEvent parsedEvent = (JSONEvent) parser.parse(event);
		JsonObject jsonObject = parsedEvent.getJsonObject();
		
		Assert.assertNotNull(jsonObject.get("event_timestamp"));
		Assert.assertEquals("2016-11-02T09:12:09+0100", jsonObject.get("event_timestamp").getAsString());
		Assert.assertNotNull(jsonObject.get("text"));
		Assert.assertEquals(text, jsonObject.get("text").getAsString());
		
		Assert.assertNotNull(jsonObject.get("CONNECT_DATA_SERVER"));
		Assert.assertEquals("DEDICATED", jsonObject.get("CONNECT_DATA_SERVER").getAsString());
		Assert.assertNotNull(jsonObject.get("CONNECT_DATA_SERVICE_NAME"));
		Assert.assertEquals("timtest_s.cern.ch", jsonObject.get("CONNECT_DATA_SERVICE_NAME").getAsString());
		Assert.assertNotNull(jsonObject.get("CONNECT_DATA_FAILOVER_MODE_TYPE"));
		Assert.assertEquals("SELECT", jsonObject.get("CONNECT_DATA_FAILOVER_MODE_TYPE").getAsString());
		Assert.assertNotNull(jsonObject.get("CONNECT_DATA_FAILOVER_MODE_METHOD"));
		Assert.assertEquals("BASIC", jsonObject.get("CONNECT_DATA_FAILOVER_MODE_METHOD").getAsString());
		Assert.assertNotNull(jsonObject.get("CONNECT_DATA_FAILOVER_MODE_RETRIES"));
		Assert.assertEquals(200, jsonObject.get("CONNECT_DATA_FAILOVER_MODE_RETRIES").getAsInt());
		Assert.assertNotNull(jsonObject.get("CONNECT_DATA_FAILOVER_MODE_DELAY"));
		Assert.assertEquals(15, jsonObject.get("CONNECT_DATA_FAILOVER_MODE_DELAY").getAsInt());
		Assert.assertNotNull(jsonObject.get("client_program"));
		Assert.assertEquals("perl", jsonObject.get("client_program").getAsString());
		Assert.assertNotNull(jsonObject.get("client_host"));
		Assert.assertEquals("itrac50044.cern.ch", jsonObject.get("client_host").getAsString());
		Assert.assertNotNull(jsonObject.get("client_user"));
		Assert.assertEquals("oracle", jsonObject.get("client_user").getAsString());
		Assert.assertNotNull(jsonObject.get("CONNECT_DATA_INSTANCE_NAME"));
		Assert.assertEquals("ACCINT2", jsonObject.get("CONNECT_DATA_INSTANCE_NAME").getAsString());
		
		Assert.assertNotNull(jsonObject.get("client_protocol"));
		Assert.assertEquals("tcp", jsonObject.get("client_protocol").getAsString());
		Assert.assertNotNull(jsonObject.get("client_ip"));
		Assert.assertEquals("10.16.8.163", jsonObject.get("client_ip").getAsString());
		Assert.assertNotNull(jsonObject.get("client_port"));
		Assert.assertEquals(57543, jsonObject.get("client_port").getAsInt());
		
		Assert.assertNotNull(jsonObject.get("type"));
		Assert.assertEquals("establish", jsonObject.get("type").getAsString());
		Assert.assertNotNull(jsonObject.get("service_name"));
		Assert.assertEquals("timtest_s.cern.ch", jsonObject.get("service_name").getAsString());
		Assert.assertNotNull(jsonObject.get("return_code"));
		Assert.assertEquals(0, jsonObject.get("return_code").getAsInt());
		
		Assert.assertEquals(18, jsonObject.entrySet().size());
	}
	
	@Test
	public void service_update_extractProperties() throws ParseException{
		
		String text = "02-NOV-2016 09:12:14 "
				+ "* service_update "
				+ "* INT8R2 "
				+ "* 0";
				
		LogEvent event = new LogEvent(ListenerLogEventParser.LISTENER_LOGS_DATE_FORMAT, text);
		
		ListenerLogEventParser parser = 
				(ListenerLogEventParser) new ListenerLogEventParser.Builder().build(null);
		
		JSONEvent parsedEvent = (JSONEvent) parser.parse(event);
		JsonObject jsonObject = parsedEvent.getJsonObject();
		
		Assert.assertNotNull(jsonObject.get("event_timestamp"));
		Assert.assertEquals("2016-11-02T09:12:14+0100", jsonObject.get("event_timestamp").getAsString());
		Assert.assertNotNull(jsonObject.get("text"));
		Assert.assertEquals(text, jsonObject.get("text").getAsString());
		
		Assert.assertNotNull(jsonObject.get("type"));
		Assert.assertEquals("service_update", jsonObject.get("type").getAsString());
		Assert.assertNotNull(jsonObject.get("service_name"));
		Assert.assertEquals("INT8R2", jsonObject.get("service_name").getAsString());
		Assert.assertNotNull(jsonObject.get("return_code"));
		Assert.assertEquals(0, jsonObject.get("return_code").getAsInt());
		
		Assert.assertEquals(5, jsonObject.entrySet().size());
	}
	
	@Test
	public void status_extractProperties() throws ParseException{
		
		String text = "02-NOV-2016 09:12:08 "
				+ "* (CONNECT_DATA="
					+ "(CID="
						+ "(PROGRAM=)"
						+ "(HOST=itrac1311.cern.ch)"
						+ "(USER=oracle))"
					+ "(COMMAND=status)"
					+ "(ARGUMENTS=64)"
					+ "(SERVICE=LISTENER_SCAN1)"
					+ "(VERSION=202375424)) "
				+ "* status "
				+ "* 5";
				
		LogEvent event = new LogEvent(ListenerLogEventParser.LISTENER_LOGS_DATE_FORMAT, text);
		
		ListenerLogEventParser parser = 
				(ListenerLogEventParser) new ListenerLogEventParser.Builder().build(null);
		
		JSONEvent parsedEvent = (JSONEvent) parser.parse(event);
		JsonObject jsonObject = parsedEvent.getJsonObject();
		
		Assert.assertNotNull(jsonObject.get("event_timestamp"));
		Assert.assertEquals("2016-11-02T09:12:08+0100", jsonObject.get("event_timestamp").getAsString());
		Assert.assertNotNull(jsonObject.get("text"));
		Assert.assertEquals(text, jsonObject.get("text").getAsString());
		
		Assert.assertNotNull(jsonObject.get("client_program"));
		Assert.assertEquals("", jsonObject.get("client_program").getAsString());
		Assert.assertNotNull(jsonObject.get("client_host"));
		Assert.assertEquals("itrac1311.cern.ch", jsonObject.get("client_host").getAsString());
		Assert.assertNotNull(jsonObject.get("client_user"));
		Assert.assertEquals("oracle", jsonObject.get("client_user").getAsString());
		Assert.assertNotNull(jsonObject.get("CONNECT_DATA_COMMAND"));
		Assert.assertEquals("status", jsonObject.get("CONNECT_DATA_COMMAND").getAsString());
		Assert.assertNotNull(jsonObject.get("CONNECT_DATA_ARGUMENTS"));
		Assert.assertEquals(64, jsonObject.get("CONNECT_DATA_ARGUMENTS").getAsInt());
		Assert.assertNotNull(jsonObject.get("CONNECT_DATA_SERVICE"));
		Assert.assertEquals("LISTENER_SCAN1", jsonObject.get("CONNECT_DATA_SERVICE").getAsString());
		Assert.assertNotNull(jsonObject.get("CONNECT_DATA_VERSION"));
		Assert.assertEquals(202375424, jsonObject.get("CONNECT_DATA_VERSION").getAsInt());
		
		Assert.assertNotNull(jsonObject.get("type"));
		Assert.assertEquals("status", jsonObject.get("type").getAsString());
		Assert.assertNotNull(jsonObject.get("return_code"));
		Assert.assertEquals(5, jsonObject.get("return_code").getAsInt());
		
		Assert.assertEquals(11, jsonObject.entrySet().size());
	}
	
	@Test
	public void ping_extractProperties() throws ParseException{
		
		String text = "02-NOV-2016 09:12:06 * ping * 15";
				
		LogEvent event = new LogEvent(ListenerLogEventParser.LISTENER_LOGS_DATE_FORMAT, text);
		
		ListenerLogEventParser parser = 
				(ListenerLogEventParser) new ListenerLogEventParser.Builder().build(null);
		
		JSONEvent parsedEvent = (JSONEvent) parser.parse(event);
		JsonObject jsonObject = parsedEvent.getJsonObject();
		
		Assert.assertNotNull(jsonObject.get("event_timestamp"));
		Assert.assertEquals("2016-11-02T09:12:06+0100", jsonObject.get("event_timestamp").getAsString());
		Assert.assertNotNull(jsonObject.get("text"));
		Assert.assertEquals(text, jsonObject.get("text").getAsString());
		
		Assert.assertNotNull(jsonObject.get("type"));
		Assert.assertEquals("ping", jsonObject.get("type").getAsString());
		Assert.assertNotNull(jsonObject.get("return_code"));
		Assert.assertEquals(15, jsonObject.get("return_code").getAsInt());
		
		Assert.assertEquals(4, jsonObject.entrySet().size());
	}
	
	@Test
	public void services_extractProperties() throws ParseException{
		
		String text = "15-NOV-2016 13:34:06 "
				+ "* (CONNECT_DATA=(CID=(PROGRAM=)(HOST=itrac1316.cern.ch)(USER=oracle))"
					+ "(COMMAND=services)(ARGUMENTS=64)(SERVICE=(ADDRESS=(PROTOCOL=TCP)(HOST=itrac1316-v.cern.ch)"
					+ "(PORT=10121)))(VERSION=202375680)) "
				+ "* services "
				+ "* 0";
				
		LogEvent event = new LogEvent(ListenerLogEventParser.LISTENER_LOGS_DATE_FORMAT, text);
		
		ListenerLogEventParser parser = 
				(ListenerLogEventParser) new ListenerLogEventParser.Builder().build(null);
		
		JSONEvent parsedEvent = (JSONEvent) parser.parse(event);
		JsonObject jsonObject = parsedEvent.getJsonObject();
		
		Assert.assertNotNull(jsonObject.get("event_timestamp"));
		Assert.assertEquals("2016-11-15T13:34:06+0100", jsonObject.get("event_timestamp").getAsString());
		Assert.assertNotNull(jsonObject.get("text"));
		Assert.assertEquals(text, jsonObject.get("text").getAsString());
		
		Assert.assertNotNull(jsonObject.get("type"));
		Assert.assertEquals("services", jsonObject.get("type").getAsString());
		Assert.assertNotNull(jsonObject.get("return_code"));
		Assert.assertEquals(0, jsonObject.get("return_code").getAsInt());
		
		Assert.assertEquals(13, jsonObject.entrySet().size());
	}
	
	@Test
	public void other_extractProperties() throws ParseException{
		
		String text = "02-NOV-2016 09:12:06 * unknown * other_type * 15";
				
		LogEvent event = new LogEvent(ListenerLogEventParser.LISTENER_LOGS_DATE_FORMAT, text);
		
		ListenerLogEventParser parser = 
				(ListenerLogEventParser) new ListenerLogEventParser.Builder().build(null);
		
		JSONEvent parsedEvent = (JSONEvent) parser.parse(event);
		JsonObject jsonObject = parsedEvent.getJsonObject();
		
		Assert.assertNotNull(jsonObject.get("event_timestamp"));
		Assert.assertEquals("2016-11-02T09:12:06+0100", jsonObject.get("event_timestamp").getAsString());
		Assert.assertNotNull(jsonObject.get("text"));
		Assert.assertEquals(text, jsonObject.get("text").getAsString());
		
		Assert.assertEquals(2, jsonObject.entrySet().size());
	}
	
	@Test
	public void wrongLogFormat() throws ParseException{
		
		String text = "02-NOV-2016 09:12:06, unknown, other_type, 15";
				
		LogEvent event = new LogEvent(ListenerLogEventParser.LISTENER_LOGS_DATE_FORMAT, text);
		
		ListenerLogEventParser parser = 
				(ListenerLogEventParser) new ListenerLogEventParser.Builder().build(null);
		
		JSONEvent parsedEvent = (JSONEvent) parser.parse(event);
		JsonObject jsonObject = parsedEvent.getJsonObject();
		
		Assert.assertNotNull(jsonObject.get("event_timestamp"));
		Assert.assertEquals("2016-11-02T09:12:06+0100", jsonObject.get("event_timestamp").getAsString());
		Assert.assertNotNull(jsonObject.get("text"));
		Assert.assertEquals(text, jsonObject.get("text").getAsString());
		
		Assert.assertEquals(2, jsonObject.entrySet().size());
	}
	
	@Test
	public void eventsFromLogFileSource() throws InterruptedException{
		Context context = new Context();
		context.put(ReliableLogFileEventReader.LOG_FILE_PATH_PARAM, 
				new File("src/test/resources/sample-logs/listener.log").getAbsolutePath());
		context.put(ReliableLogFileEventReader.DATAFORMAT_PARAM, "dd-MMM-yyy HH:mm:ss");
		context.put(ReliableLogFileEventReader.LOG_EVENTS_WITH_SEVERAL_LINES_PARAM, "false");
		
		context.put(ReliableLogFileEventReader.PARSER_PARAM, ListenerLogEventParser.Builder.class.getName());
		
		LogFileSource source = new LogFileSource();
		source.configure(context);
		
		Map<String, String> channelContext = new HashMap<String, String>();
	    channelContext.put("capacity", "100");
	    channelContext.put("keep-alive", "0"); // for faster tests
	    Channel channel = new MemoryChannel();
	    Configurables.configure(channel, new Context(channelContext));
	    
	    ChannelSelector rcs = new ReplicatingChannelSelector();
	    rcs.setChannels(Collections.singletonList(channel));
	    ChannelProcessor chp = new ChannelProcessor(rcs);
	    chp.configure(context);
	    source.setChannelProcessor(chp);
	    
	    PollableSourceRunner runner = new PollableSourceRunner();
	    runner.setSource(source);
	    runner.start();
	    
	    Thread.sleep(500);
	    
	    channel.getTransaction().begin();
	    	    
	    // Read a few events
	    
//	    29-JUL-2016 15:17:34 * (CONNECT_DATA=(SID=DESFOUND)(CID=(PROGRAM=oracle)(HOST=itrac50035.cern.ch)(USER=oracle))) 
//	    * (ADDRESS=(PROTOCOL=tcp)(HOST=137.138.147.42)(PORT=30816)) * establish * DESFOUND * 0
	    JSONEvent event = (JSONEvent) channel.take();
	    JsonObject jsonObject = event.getJsonObject();
  		Assert.assertEquals("2016-07-29T15:17:34+0200", jsonObject.get("event_timestamp").getAsString());
  		Assert.assertEquals("itrac50035.cern.ch", jsonObject.get("client_host").getAsString());
  		Assert.assertEquals("establish", jsonObject.get("type").getAsString());
  		Assert.assertEquals("DESFOUND", jsonObject.get("service_name").getAsString());
  		Assert.assertEquals(0, jsonObject.get("return_code").getAsInt());
  		
//	    29-JUL-2016 15:17:38 * service_update * WCERND * 0
  		event = (JSONEvent) channel.take();
	    jsonObject = event.getJsonObject();
  		Assert.assertEquals("2016-07-29T15:17:38+0200", jsonObject.get("event_timestamp").getAsString());
  		Assert.assertEquals("service_update", jsonObject.get("type").getAsString());
  		Assert.assertEquals("WCERND", jsonObject.get("service_name").getAsString());
  		Assert.assertEquals(0, jsonObject.get("return_code").getAsInt());

//	    29-JUL-2016 15:18:45 * (CONNECT_DATA=(SID=PAYT)(CID=(PROGRAM=RTSDGN@db-50016)(HOST=db-50016)(USER=paytest))) 
//  	* (ADDRESS=(PROTOCOL=tcp)(HOST=10.18.16.71)(PORT=47994)) * establish * PAYT * 0
  		event = (JSONEvent) channel.take();
	    jsonObject = event.getJsonObject();
  		Assert.assertEquals("2016-07-29T15:18:45+0200", jsonObject.get("event_timestamp").getAsString());
  		Assert.assertEquals("RTSDGN@db-50016", jsonObject.get("client_program").getAsString());
  		Assert.assertEquals("establish", jsonObject.get("type").getAsString());
  		Assert.assertEquals("PAYT", jsonObject.get("service_name").getAsString());
  		Assert.assertEquals(0, jsonObject.get("return_code").getAsInt());

//	    29-JUL-2016 15:19:55 * (CONNECT_DATA=(SID=PAYT)(CID=(PROGRAM=RTSDGN@db-50016)(HOST=db-50016)(USER=paytest))) 
//  	* (ADDRESS=(PROTOCOL=tcp)(HOST=10.18.16.71)(PORT=48029)) * establish * PAYT * 0
  		event = (JSONEvent) channel.take();
	    jsonObject = event.getJsonObject();
  		Assert.assertEquals("2016-07-29T15:19:55+0200", jsonObject.get("event_timestamp").getAsString());
  		Assert.assertEquals("RTSDGN@db-50016", jsonObject.get("client_program").getAsString());
  		Assert.assertEquals("establish", jsonObject.get("type").getAsString());
  		Assert.assertEquals("PAYT", jsonObject.get("service_name").getAsString());
  		Assert.assertEquals(0, jsonObject.get("return_code").getAsInt());
  		
//	    29-JUL-2016 15:19:56 * (CONNECT_DATA=(SID=PAYT)(CID=(PROGRAM=sqlplus)(HOST=db-50016)(USER=paytest))) 
//  	* (ADDRESS=(PROTOCOL=tcp)(HOST=10.18.16.71)(PORT=48033)) * establish * PAYT * 0
  		event = (JSONEvent) channel.take();
	    jsonObject = event.getJsonObject();
  		Assert.assertEquals("2016-07-29T15:19:56+0200", jsonObject.get("event_timestamp").getAsString());
  		Assert.assertEquals("sqlplus", jsonObject.get("client_program").getAsString());
  		Assert.assertEquals("establish", jsonObject.get("type").getAsString());
  		Assert.assertEquals("PAYT", jsonObject.get("service_name").getAsString());
  		Assert.assertEquals(0, jsonObject.get("return_code").getAsInt());
	    
	    channel.getTransaction().commit();
	    channel.getTransaction().close();
	    
	    runner.stop();
	}
	
	@After
	public void cleanUp(){
		new File(ReliableLogFileEventReader.COMMITTING_FILE_PATH_DEFAULT).delete();
		new File(DropDuplicatedEventsProcessor.PATH_DEFAULT).delete();
	}
	
}
